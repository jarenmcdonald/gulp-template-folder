const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');
const concat = require('gulp-concat');

/*
-- Top level functions --

 	gulp.task - defines tasks
 	gulp.src - points to files to use
	gulp.dest - points to folder for output
	gulp.watch - watch files and folders for changes

*/

//Logs message
gulp.task('message', function(){
	return console.log("Gulp is running...");
});

//Copy all HTML files into dist folder
gulp.task('copyHtml', function(){
	gulp.src('src/*.html')
		.pipe(gulp.dest('dist'));
});

//Optimize images
gulp.task('imagemin', () =>
	gulp.src('src/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
);

//Compile sass
gulp.task('sass', function(){
	gulp.src('src/sass/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('dist/css'));
});

//Concat scripts
gulp.task('concat', function(){
	gulp.src('src/js/*.js')
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dist/js'));
});

//Default task runner
gulp.task('default', ['message', 'copyHtml', 'imagemin', 'sass', 'concat']);

gulp.task('watch', function(){
	gulp.watch('src/js/*.js', ['concat']);
	gulp.watch('src/images/*', ['imagemin']);
	gulp.watch('src/sass/*.scss', ['sass']);
	gulp.watch('src/*.html', ['copyHtml']);
});
