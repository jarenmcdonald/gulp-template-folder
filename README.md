
# Gulp Template Folder

This README is for Jaren McDonald's Gulp Template folder. This folder can be used for most website development projects. This has been configured to help developers run basic website building functions as the building process continues.

## What is this repository for?

This is the source where web developers can come to clone this template folder before starting a website build. Other users can also request chages, additions, and updates as they see fit. This repository will also serve to collect any bugs or issues found by other users.

---

## How to use this template folder

### Setup

Please install ruby, if not already installed:
*	To check and see if ruby was previously installed, use the command `$ which ruby` and/or use `$ ruby --version` to ensure it is up-to-date.

Once ruby has been installed, please clone the repository from GitLab:
```
$ git clone https://gitlab.com/j.mcdonald12
```

Once the project as been cloned, open terminal and move to the root directory of the folder.
*	Use the command `$ cd ` + the path to your folder (ex. `$ cd /Desktop/gulp-template`).

Please install Node.JS and NPM if not already installed.

*	In the terminal, use the command `$ node -v` to verify is Node is installed;
	*	If not installed, please download the files from [nodejs.org](https://nodejs.org/en/);
	*	Run the installer and follow the prompts;
	*	Test that Node has installed correctly by using `$ node -v` in the terminal window;
	*	Test that Node has installed correctly by using `$ npm -v` in the terminal window.

Please install Gulp on your computer if not already installed.

*	To install Gulp globally on your computer, go to the terminal and enter `$ npm install --global gulp-cli`.

Once all of the above have been installed, you can begin to install the project dependencies.

* In a terminal window, ensure you are in the root directory of the project folder, the run `$ npm init`;
* Check the package.json file to see all saved dependencies.


### Running Gulp

Before using this template, please ensure you are not working from this repository, but have copied the folder locally and initialized your own project. This will keep your additional project updates out of this template repo.

In order to properly use this template, you should never work inside the *__dist__* folder. Instead, all of the development work must be done in the *__src__* folder. This is because the taskrunner has been configured to move the working files from within the *__src__* folder over to the *__dist__* folder, and not the other way around.

There are several ways to start the taskrunner, but only one should be needed to work within the project:

*	Open a terminal window
*	Move to the root directory of the project folder
	*	`$ cd /pathToYourWorkingFolder/gulp-template`
*	Enter `gulp watch`

Gulp will continue to watch for changes as you work and continuously save your files.

To stop watching for changes, in the terminal window type `$ ⌃c`.

---

## Contribution guidelines

Please feel free to look for ways this folder and it's configuration can improve.

If you believe you have found an error or issue:

1.	Submit a ticket for your issue, assuming one does not already exist;
2.	Clearly describe the issue including steps to reproduce when it is a bug;
3.	Make sure you fill in the earliest version that you know has the issue;
4.	Fork the repository on GitLab.

#### Making Changes:

__Create a topic branch from where you want to base your work.__

*	This is usually the master branch;
*	Only target release branches if you are certain your fix must be on that 	branch;
*	Please avoid working directly on the master branch;
*	Make commits of logical units;
*	Check for unnecessary whitespace with `$ git diff --check` before committing;
*	Make sure your commit messages are in the proper format;
*	Make sure you have added the necessary tests for your changes;
*	Run all the tests to assure nothing else was accidentally broken.

---

## About this project

### Author

| Name             | Position            |Portfolio
| ---------------- | --------------------| ---------------------
| Jaren McDonald   | Front-End Developer | [jaren.ca](https://jaren.ca/)

### Who do I talk to?

For any questions or comments, feel free to myself, Jaren.
